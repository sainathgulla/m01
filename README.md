# M01 - Chrome, Sublime #

## First, we'll get some basic tools set up.##  

Keep track of how long it takes to complete this assignment. 

* Install Google Chrome browser.
* Install Sublime Text 3 (https://www.sublimetext.com/3) text editor.
* Create a static web page as your introduction:
* Create a folder on your C: drive named 44563. 
* Create a subfolder under that named M01. 
* Create a new empty text file named Lastname_Firstname.html. 
* Open this file in Sublime.
* Type HTML and hit TAB to initialize a new HTML5 file. 
* Title your page with just your preferred name - don't include your family / last name, e.g. <title>Samuel</title>.
* In the body, using basic HTML tags, provide your introduction. Using headings, lists, tables, and/or content sections, introduce yourself by telling us a bit about your background (where you came from) and where you're going (what kind of work you want, etc).  Include any experience, interests, home town or school, or other information to help us get to know you. 

### To Submit ###

* Title your post "NN Lastname, Firstname - success" if successful.  Title your post "NN Lastname, Firstname" if you run into any problems or have any questions. NN is your two-digit section, e.g. 03. Incorrect format on the post title = -10%. . 
* Embed a screenshot showing your (unstyled) introduction web page running in Chrome on your desktop. 
* Copy and paste your html code into your post. (Use a screenshot if needed.)
* Tell us how long the assignment took. 
* Tell us if you had any problems or if you have any questions. 
* Clarification: Do not attach files - display all images and code in your post (see the icon that looks like ).  Save your screenshot, upload it to tinypic.com. Enter the passphrase and select the last option "Direct link for layouts."